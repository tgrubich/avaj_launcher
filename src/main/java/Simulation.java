public class Simulation {
    private int id;
    private String type;
    private String name;
    private int longitude;
    private int latitude;
    private int height;

    public Simulation(int id, String type, String name, int longitude, int latitude, int height) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.height = height;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public int getLongitude() {
        return longitude;
    }

    public int getLatitude() {
        return latitude;
    }

    public int getHeight() {
        return height;
    }
}
