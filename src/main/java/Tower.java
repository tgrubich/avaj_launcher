import java.util.ArrayList;
import java.util.List;

public class Tower {


    private List<Flyable> flyable = new ArrayList<>();

    public void register(Flyable newFlyable) {
        if (!flyable.contains(newFlyable)) {
            flyable.add(newFlyable);
        }
        if (flyable.size() > 1) {
            CheckCoordinates.deleteAircraftCrash(this);
        }

    }

    public void print() {
        for (int i = 0; i < flyable.size(); i++) {
            System.out.println(flyable.get(i));
        }
    }

    public void unregister(Flyable oldFlyable) {
        flyable.remove(oldFlyable);
        System.out.println(oldFlyable +  "lending");
        System.out.println("Tower says: " + oldFlyable + "unregistered from weather tower.");
    }

    public void unregisterCrash(Flyable oldFlyable) {
        flyable.remove(oldFlyable);
        System.out.println("Tower says: " + oldFlyable + " crashed!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("Tower says: " + oldFlyable + "unregistered from weather tower.");

    }

    protected void conditionChanged() {
        for (int i = 0; i < flyable.size(); i++) {
            Flyable fly = flyable.get(i);
            fly.updateConditions();
            if (!CheckCoordinates.checkCoordinate(fly.getCoordinates())) {
                unregister(fly);
            }
        }
        CheckCoordinates.deleteAircraftCrash(this);
    }

    public List<Flyable> getFlyable() {
        return flyable;
    }

}
