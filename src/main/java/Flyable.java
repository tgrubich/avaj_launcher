
public interface Flyable {

    Coordinates getCoordinates();
    void updateConditions();
    void registerTower(WeatherTower weatherTower);

}
