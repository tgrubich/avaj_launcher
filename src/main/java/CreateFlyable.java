import java.util.ArrayList;
import java.util.List;

public class CreateFlyable {

    List<Flyable> flyableList = new ArrayList<>();

    public List<Flyable> createFlyable(List<Simulation> simulationList) {

        for (int i = 0; i < simulationList.size(); i++) {
            Flyable flyable = AircraftFactory.newAircraft(
                    simulationList.get(i).getType(), simulationList.get(i).getName(),
                    simulationList.get(i).getLongitude(), simulationList.get(i).getLatitude(),
                    simulationList.get(i).getHeight()
            );
            flyableList.add(flyable);
        }

        return flyableList;
    }
}
